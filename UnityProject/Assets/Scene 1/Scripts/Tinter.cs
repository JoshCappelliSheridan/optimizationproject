﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Color randomColor = new Color(UnityEngine.Random.Range(0f, 1f), 
            UnityEngine.Random.Range(0f, 1f), 
            UnityEngine.Random.Range(0f, 1f));

        Mesh m_Mesh = GetComponent<MeshFilter>().mesh;
        Color[] colors = new Color[m_Mesh.vertices.Length];

        for(int i = 0; i < colors.Length; i++)
        {
            colors[i] = randomColor;
        }

        m_Mesh.colors = colors;
    }

}
