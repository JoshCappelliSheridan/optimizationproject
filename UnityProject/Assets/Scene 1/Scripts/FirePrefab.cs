﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )] //Make sure this is attactched to the camera
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public int poolSize = 1000;
    public float FireSpeed = 5;

    Camera mainCamera;

    Rigidbody[] objectPool;
    int iterator = 0;

    void Start()
    {
        mainCamera = GetComponent<Camera>();

        objectPool = new Rigidbody[poolSize];

        for (int i = 0; i < poolSize; i++)
        {
            GameObject newObj = Instantiate(Prefab) as GameObject;
            Rigidbody newRb = newObj.GetComponent<Rigidbody>();

            newObj.SetActive(false);

            objectPool[i] = newRb;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetButton( "Fire1" ) ) // When Fire Button is Down
        {
            // Get World space position of where the mouse cursor is
            Vector3 clickPoint = mainCamera.ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            // Set fire direction to point away from the camera
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();

            Rigidbody rb = objectPool[iterator];
            rb.gameObject.SetActive(true);
            rb.position = transform.position;
            rb.velocity = FireDirection * FireSpeed;

            iterator++;
            if (iterator > poolSize - 1)
                iterator = 0;
        }
    }

}
