﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    public LayerMask navigationMask;
    Camera m_Camera;
    NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        // Get Main Camera
        m_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

	// Update is called once per frame
	void Update ()
    {
        // Create raycast hit variable
        RaycastHit hit = new RaycastHit();
        Ray camRay = m_Camera.ScreenPointToRay(Input.mousePosition);

        // Raycast from the camera to where the mouse cursor is
        if (Physics.Raycast(camRay,out hit,float.MaxValue,navigationMask))
        {
            //Set NavMesh Target
            agent.destination = hit.point;
        }
	}
}
