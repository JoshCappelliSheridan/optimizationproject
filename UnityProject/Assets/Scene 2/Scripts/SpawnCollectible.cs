﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        if ( m_currentCollectible == null ) // check if there is a collectible
        {
            //If there is not, create a new one in the position of one of my child transforms
            m_currentCollectible = Instantiate( Collectible, 
                this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
