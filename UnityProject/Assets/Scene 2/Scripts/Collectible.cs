﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void Update()
    {
        Vector3 vectorToPlayer = player.position - transform.position;

        if ( vectorToPlayer.sqrMagnitude < Radius * Radius) //Check for player tag
        {
            Destroy( this.gameObject ); //Destroy Self
        }
    }

}
